///////////////////////////////////////////////////
//SC-285GP

//------------------------------------------------------
//setting data

#define TEM_DEFAULT                 (3)
#define TEM_MIN                     (2)
#define TEM_MAX                     (6)
#define TEM_UP_MISTAKE_DEFAULT      15
#define TEM_UP_MISTAKE_MIN          0
#define TEM_UP_MISTAKE_MAX          99
#define TEM_DN_MISTAKE_DEFAULT      15
#define TEM_DN_MISTAKE_MIN          0
#define TEM_DN_MISTAKE_MAX          99
#define TEM_SENSOR_ERR_OPEN_TIME_DEFAULT    10
#define TEM_SENSOR_ERR_OPEN_TIME_MIN        0
#define TEM_SENSOR_ERR_OPEN_TIME_MAX        99
#define TEM_SENSOR_ERR_CLOSE_TIME_DEFAULT    10
#define TEM_SENSOR_ERR_CLOSE_TIME_MIN        1
#define TEM_SENSOR_ERR_CLOSE_TIME_MAX        99
#define TEM_COMPENSATE_DEFAULT     -10
#define TEM_COMPENSATE_MIN         (-99)
#define TEM_COMPENSATE_MAX         99


#define TEM_DEFROSTING_CYCLE_DEFAULT  6
#define TEM_DEFROSTING_CYCLE_MIN  0
#define TEM_DEFROSTING_CYCLE_MAX  99

#define TEM_DEFROSTING_TIME_DEFAULT  25
#define TEM_DEFROSTING_TIME_MIN  1
#define TEM_DEFROSTING_TIME_MAX  99

#define TEM_DIS_MODE_DEFAULT       0
#define TEM_DIS_MODE_MIN       0
#define TEM_DIS_MODE_MAX       2

#define TEM_COMP_STOP_MIN      3
#define TEM_HHH_ALARM            15
#define TEM_LLL_ALARM           -10
#define TEM_UPLOADNET_CYCLE    30
#define TEM_HEARTBEAT_CYCLE    30


enum{
	PARA_TEM = 0,
	PARA_C1,
	PARA_C2,
	PARA_C3,
	PARA_C4,
	PARA_C5,
	PARA_D1,
	PARA_D2,
	PARA_F1,
	PARA_F2,
	PARA_F3,

	PARA_COMP_STOP_MIN,
	PARA_TS_MAX,
	PARA_TS_MIN,
	PARA_HHH_ALARM,
	PARA_LLL_ALARM,
	PARA_UPLOADNET_CYCLE,
	PARA_HEARTBEAT_CYCLE,
};

typedef struct {
	struct rw{
		int32_t tem;           //设定温度值 //2~6度 3 =ts温度
		int32_t tem_up_mistake;//C1温度上回差 0~99 15 0.1
		int32_t tem_dn_mistake;//C2温度下回差 0~99 15 0.1
		int32_t tem_sensor_err_open_time;  //C3传感器故障压机开机时间 0~99 10 1m
		int32_t tem_sensor_err_close_time; //C4传感器故障压机关机时间 1~99 10 1m
		int32_t tem_compensate;//C5温度补偿值 -99~99 0 单位0.1
		int32_t tem_defrosting_cycle;//除霜周期 0～99  6 1h
		int32_t tem_defrosting_time; //除霜时间 1～99  25 1m
		int32_t tem_fault_code;//故障代码查看
		int32_t tem_gprs_signal_intensity;//GPRS 信号强度查看
		int32_t tem_dis_mode;  //F1显示方式 0~2 0
		uint32_t crc;
	} rw;
	struct readonly{
		int32_t compressor_working_time_MAX;//压机最长连续运行时间 --不可调 6h
		int32_t compressor_working_overtime_stop_MAX;//压机运行超时强制停机时间 --不可调 15m
		int32_t compressor_rest_time_stop_MIN;       //压机最小停机时间 --不可调 3m
		int32_t tem_setMAX;    //温度设定上限 --不可调 -18
		int32_t tem_setMIN;    //温度设定下限 --不可调 -28
		int32_t tem_HHH_alarm; //高温报警值 --可远程调节 TS+1~-5
		int32_t tem_LLL_alarm; //低温报警值 --可远程调节 -45~TS-1
		int32_t tem_overtem_duration_time;//超温持续时间
		int32_t temp32;//临时用数据 (目前主要用于存二级菜单目录值C1-F1)
		int32_t menu_counter;  //计本层菜单的退出时间。
		uint8_t menu_value;//菜单当前值
		uint16_t tem_uploadnet_cycle;//30m//30 分钟（仅可通过服务器远程调节）
		uint16_t tem_heartbeat_cycle;//10m//10 分钟（仅可通过服务器远程调节）V1.0.11修改成30M
	} ro;
} setdata_t;


int32_t setdata_get_para( uint32_t idx );
void setdata_set_para( uint32_t idx, int32_t value );

