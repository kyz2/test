#ifndef __TEM_H__
#define __TEM_H__

///////////////////////////////////////////////////
//SC-285GP

//------------------------------------------------------
//setting data


void disp_tem_adjust_init( void );
void disp_tem_adjust( void );

void alarm_state_init( void );
void alarm_state_update( void );

void compressor_state_init( void );
void compressor_state_update( void );


//------------------------------------------------------
// disp tem info
typedef enum{
	ADJUST_OFF,
	ADJUST_STOP,
	ADJUST_M0_HIGH,
	ADJUST_M0_HIGH_STOP,
	ADJUST_M0_LOW,
	ADJUST_M0_LOW_STOP,
}adjust_st_t;

typedef struct{
	int32_t measure_tem;    //测量温度
	int32_t to_disp_tem;    //目标显示温度
	int32_t disp_tem;       //显示温度

	struct adj{
		adjust_st_t st;
		uint32_t start;
		uint32_t last;
	}adjust;
}tem_disp_t;

void temdisp_adjust_init( void );
void temdisp_adjust( void );
void temdisp_mode_change( uint32_t mode );

//------------------------------------------------------
#if XXX

//制冷控制
// TSK = TS + C1  
// TSG = TS - C2

//STATE
//参数定义
//	C1 温度上回差                    0~99 15 0.1°C 可调
//	C2 温度下回差                    0~99 15 0.1°C 可调
//	C3 传感器故障时压缩机开机时间    0~99 10 1 分钟 可调
//	C4 传感器故障时压缩机停机时间    1~99 10 1 分钟 可调
//	C5 温度补偿                    -99~99 -10 0.1°C 可调
//	d1 除霜周期                      0~99 6 1 小时 可调
//	d2 除霜时间                      1~99 25 1 分钟 可调
//	F1 故障代码查看                  / / / 参考 3.4
//	F2 GPRS 信号强度查看             / / / 参考 3.5
//	F3 显示方式                      0~2 显示方式 0 / 可调

typedef struct{

//tem display
int32_t real_tem;
int32_t disp_tem;
int32_t target_disp_tem;

uint32_t disp_mode;
uint32_t amend;//
uint32_t amend_time; //0:stop amend, else: amend start time;

//alarm info

}teminfo_t;

void tem_timer_handler( void)
{
	adc = readadc();
	real_t = adc_to_t();

	tem_adjust = 0;

	if tem_adjust = 0
		if( t_time > 6H ||  abs(  real_tem - t )< 2 )
			tem_adjust = 1;

	if tem_adjust == 0 
		disp_tem = real_tem; //7.2
	else{

		if real_t < t-2 ){
			//7.5
		}else if real_t > t+2 ){
			//7.4
			new_disp = t+1;
			if( now->disp_time < 5M )
				new_disp = time_v/60*1c
			else
				new_disp += 5+(time_V-5M)/10
			if new_disp > real_t
				target_disp = real_t;
			else
				target-disp = real_t;


		}else{		
			target_disp_tem = real_tem+C5; //7.3

		}
	}
	//7.6
	if( disp_tem < 1 )
		disp_tem ==1
}
	



void alarm_mode_0_check( void )
{
	if( tem_amend == 0 )
		return;

	if( disp_tem > AL_Tem )
		HA_time++;
		if( HA_time > 1M )
			raise HA alaram
	if( AL_Threshold < 1 ){
		if( disp_tem < 1 )
			LA_time++;
			if( LA_time > 1M
				start "LA"
	}else{
		if( disp_tem < AL_threshold )
			LA_time++
				if( LA_time > 1M:
					start "LA"	
	}

	start( alarm_window);
}
#endif //XXX
#endif //__TEM_H__
