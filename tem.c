#include "ui.h"
#include "tem.h"
#include "setting.h"

extern int32_t read_tem( void );
extern uint32_t tem_sensor_status( void );

tem_disp_t g_temdisp;

//-------------------------------------------------
/*
void alarm_state_init( void );
void alarm_state_update( void );

void compressor_state_init( void );
void compressor_state_update( void );
*/

//-------------------------------------------------

extern int32_t read_temperature( void );

void temdisp_adjust_init( void )
{
	memset( &g_temdisp, 0, sizeof( g_temdisp) );
	g_temdisp.measure_tem = read_tem();
	g_temdisp.adjust.st = ADJUST_OFF;
	g_temdisp.adjust.start = 0;
	g_temdisp.adjust.last = 0;

	//if( not first_power || time() > 6H )
	//	g_temdisp.adjust.st = ADJUST_STOP;
}

/*	七、温度显示规则(显示方式 0)
	7.1、目标显示温度=实际测量温度 + 温度补偿值 C5;
	7.2、首次上电,温度修正功能屏蔽,温度显示数值为测量数值;
	7.3、当温度设定值 - 2 < 温度测量数值 < 温度设定值 + 2 时,温度修正功能打开,但在此
		区间不进行修正,显示数值 =测量数值 + C5;
	7.4、当 温度测量数值 ≥ 温度设定值 + 2 时,温度显示数值从温度设定值 + 1 开始修正,以
		后每 60S 增加 1°C,当 目标显示温度 ≤ 当前显示数值时,则显示温度为目标显示温度,停止修正。
		当修正时间超过 5 分钟,仍未达到目标数值,则开始快速修正,每 10s 增加 1°C,当 目标显示温度
		≤ 当前显示数值时,则显示温度为目标显示温度,停止修正。 当修正时间超过 8 分钟,温度显示数
		值快速达到目标显示温度,并停止温度修正。
	7.5、当 温度测量数值 ≤ 温度设定值 - 2 时,温度显示数值从温度设定值 - 1 开始修正,以后
		每 60S 减少 1 °C,当 目标显示温度 ≥ 当前显示数值时,则显示温度为目标显示温度,停止修正。
		当修正时间超过 5 分钟,仍未达到目标数值,则开始快速修正,每 10s 减少 1°C,当 目标显示温度
		≥ 当前显示数值时,则显示温度为目标显示温度,停止修正。当修正时间超过 8 分钟,温度显示数
		值快速达目标显示温度,并停止温度修正。
	7.6、最高优先级:温度显示范围为≥1°C;
*/

int32_t temdisp_adjust_high(  int32_t from, int32_t to )
{
	//7.4
	int32_t new_disp;
	int32_t to_disp;
	uint32_t time_now;

	//温度显示数值从温度设定值 + 1 开始修正
	if( from <  setdata_get_para(PARA_TEM)+10 )
		from =  setdata_get_para(PARA_TEM)+10;

	if( g_temdisp.adjust.st == ADJUST_M0_HIGH_STOP ){
		//	return to_disp;
	}

	time_now = now();
	to_disp = to;
	new_disp = from;

	if( g_temdisp.adjust.st != ADJUST_M0_HIGH ){
		printf( "[%d] adjust normal, %d -> ADJUST_M0_HIGH (from:%d, to:%d)\n",now(),g_temdisp.adjust.st , from, to );
		g_temdisp.adjust.st = ADJUST_M0_HIGH;
		g_temdisp.adjust.start = time_now;
		g_temdisp.adjust.last = g_temdisp.adjust.start;
	}
	
	if( g_temdisp.adjust.start == 0 ){
		g_temdisp.adjust.start = time_now;
		g_temdisp.adjust.last = g_temdisp.adjust.start;
	}	


	if( time_now - g_temdisp.adjust.start > 8*60*1000 ){

		new_disp = to_disp;
	}else if( time_now - g_temdisp.adjust.start > 5 * 60 *1000 ){

		if( time_now - g_temdisp.adjust.last >  10*1000 ){

			new_disp = from + 10;
			g_temdisp.adjust.last = time_now;
		}
	}else{
		if( time_now - g_temdisp.adjust.last >  60*1000 ){

			new_disp = from + 10;
			g_temdisp.adjust.last = time_now;
		}
	}

	if( new_disp > to_disp){
		//g_temdisp.adjust.st = ADJUST_M0_HIGH_STOP;
		g_temdisp.adjust.start = 0;
		g_temdisp.adjust.last = 0;
	}else{
		to_disp = new_disp;
        }

	return to_disp;
}

int32_t temdisp_adjust_low(  int32_t from, int32_t to)
{
	//7.4
	int32_t new_disp;
	int32_t to_disp;
	uint32_t time_now;

	//温度显示数值从温度设定值 + 1 开始修正
	if( from >  setdata_get_para(PARA_TEM)-10 )
		from =  setdata_get_para(PARA_TEM)-10;

	if( g_temdisp.adjust.st == ADJUST_M0_LOW_STOP){
		//	return to_disp;
	}

	time_now = now();
	to_disp = to;
	new_disp = from;

	if( g_temdisp.adjust.st != ADJUST_M0_LOW ){
		printf( "[%d] adjust normal, %d -> ADJUST_M0_LOW (from:%d, to:%d)\n",now(),g_temdisp.adjust.st , from, to );
		g_temdisp.adjust.st = ADJUST_M0_LOW;
		g_temdisp.adjust.start = time_now;
		g_temdisp.adjust.last = g_temdisp.adjust.start;
	}
	
	if( g_temdisp.adjust.start == 0 ){
		g_temdisp.adjust.start = time_now;
		g_temdisp.adjust.last = g_temdisp.adjust.start;
	}	


	if( time_now - g_temdisp.adjust.start > 8*60*1000 ){

		new_disp = to_disp;
	}else if( time_now - g_temdisp.adjust.start > 5 * 60 *1000 ){

		if( time_now - g_temdisp.adjust.last >  10*1000 ){

			new_disp = from - 10;
			g_temdisp.adjust.last = time_now;
		}
	}else{
		if( time_now - g_temdisp.adjust.last >  60*1000 ){

			new_disp = from - 10;
			g_temdisp.adjust.last = time_now;
		}
	}

	if( new_disp <= to_disp){
		//g_temdisp.adjust.st = ADJUST_M0_HIGH_STOP;
		g_temdisp.adjust.start = 0;
		g_temdisp.adjust.last = 0;
	}else{
		to_disp = new_disp;
        }

	return to_disp;
}

int32_t temdisp_adjust_normal(  int32_t from, int32_t to )
{
	//7.3
	if( g_temdisp.adjust.st != ADJUST_STOP ){
		printf( "[%d] adjust normal, %d -> ADJUST_STOP\n",now(), g_temdisp.adjust.st );
		g_temdisp.adjust.st = ADJUST_STOP;
	}
	return to;
}

void temdisp_mode_change( uint32_t mode )
{

}

void temdisp_adjust( void )
{
	int32_t measure;
	int32_t curr_disp;
	int32_t to_disp;
	int32_t new_disp;
	int32_t ts;

	uint32_t time_now;

	measure = read_temperature();

	g_temdisp.measure_tem = measure;
{
static int32_t dbg_measure = 0;
if( dbg_measure != measure )
	printf( "Measure:%d -> %d\n", dbg_measure, measure );
	dbg_measure = measure;
}
	curr_disp = g_temdisp.disp_tem;
	ts = setdata_get_para(PARA_TEM);

	switch( setdata_get_para( PARA_F3 ) ){
		case 0:
			if( g_temdisp.adjust.st == ADJUST_OFF ){
				//7.2
				to_disp = measure;
				if(  now() > 6*3600*1000 ){
					g_temdisp.adjust.st = ADJUST_STOP;
printf( "mode:%d, curr:%d, measure:%d, ts:%d\n", setdata_get_para( PARA_F3), curr_disp, measure, ts );
printf( "[%d]:ADJUST OFF: -->  6 hours timeout -> ADJUST STOP ", now());
				}else if( measure > ts-20 && measure < ts+20 ){
					g_temdisp.adjust.st = ADJUST_STOP;
printf( "mode:%d, curr:%d, measure:%d, ts:%d\n", setdata_get_para( PARA_F3), curr_disp, measure, ts );
printf( "[%d]ADJUST OFF: --> measure :%d -> ADJUST STOP ", now(), measure );
				}
			}else{
				//7.1
				to_disp = measure + setdata_get_para( PARA_C5 );
				if( measure > ts+20){//7.4
					to_disp = temdisp_adjust_high( curr_disp, to_disp );
				}else if( measure < ts-20 ){//7.5
					to_disp = temdisp_adjust_low( curr_disp, to_disp );
				}else{ //7.3
					to_disp = temdisp_adjust_normal( curr_disp,  to_disp );
				}
				//7.6
				if( to_disp < 10 )
					to_disp = 10;
			}
			g_temdisp.disp_tem = to_disp;
			{
				static int32_t dbg_t = 0;
				if( dbg_t != to_disp )
					printf( "[%d] T: %d --> %d\n", now()/1000, dbg_t, to_disp );
				dbg_t = to_disp;	
			}
			break;
		case 1:
			//8.1
			to_disp = measure + setdata_get_para( PARA_C5 );
			//8.2
			time_now = now();	
			if( g_temdisp.adjust.last == 0 )
				g_temdisp.adjust.last = time_now;

			if( to_disp > curr_disp ){
				if( time_now - g_temdisp.adjust.last > 3*1000 ){
					g_temdisp.adjust.last = time_now;
					to_disp = curr_disp+1;
					printf( "%d -> %d\n",  curr_disp, to_disp );
				}else{
					to_disp = curr_disp;
				}

			}else if( to_disp < curr_disp ){
				if( time_now - g_temdisp.adjust.last > 6*1000 ){
					g_temdisp.adjust.last = time_now;
					to_disp = curr_disp-1;
					printf( "%d -> %d\n",  curr_disp, to_disp );
				}else{
					to_disp = curr_disp;
				}

			}else{
				g_temdisp.adjust.last = time_now;
			}

			if( to_disp < 10 )
				to_disp = 10;
			g_temdisp.disp_tem = to_disp;
			{
				static int32_t dbg_t = 0;
				if( dbg_t != to_disp )
					printf( "[%d] T: %d --> %d\n", now()/1000, dbg_t, to_disp );
				dbg_t = to_disp;	
			}
			break;

		case 2:
			to_disp = setdata_get_para( PARA_TEM );
			g_temdisp.disp_tem = to_disp;
			break;
	}
}

void adc_timer_ndler( void )
{
}

