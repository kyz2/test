#include "ui.h"


void get_key( uint32_t key, uint32_t st ); 





uint8_t PT6964_Get_Key(void);
typedef struct{
	uint32_t key;
	uint32_t pressed_time;
	uint32_t repeat_time;
}key_data_t;

key_data_t g_keydata[KEY_NUM];
key_event_t g_keyevent;

int32_t g_wait_key_released[3] = {0,};
void gen_key_event( int32_t key, int32_t key_st, int32_t pressed_time )
{
	g_keyevent.evt[key].code = 1;
	g_keyevent.evt[key].st   = key_st;
	g_keyevent.evt[key].keep_time  = now()-pressed_time;

	if( g_wait_key_released[key] ){
		g_keyevent.evt[key].code = 0;
		if( key_st == 0 )
			g_wait_key_released[key] = 0;
	}
}

uint32_t get_key_event( key_event_t *k )
{
	memcpy( k, &g_keyevent, sizeof( key_event_t ) );

	memset( &g_keyevent, 0, sizeof( g_keyevent ) );
	return 1;	
};

void key_timer_handler( void )
{
	static uint32_t last_k = 0;
	uint32_t raw;
	uint32_t k_new[KEY_NUM];
	int32_t i;

	raw = PT6964_Get_Key();
	k_new[KEY_SET]   = ( raw& 1)?1:0;
	k_new[KEY_PLUS]  = ( raw& 2)?1:0;
	k_new[KEY_MINUS] = ( raw& 4)?1:0;
	

	for( i=0; i<KEY_NUM; i++ ){

		if( g_keydata[i].key && !k_new[i] ){
			//key Down -> UP
			//printf( "----KEY UP----\n" );
			gen_key_event( i, 0, g_keydata[i].pressed_time );
			g_keydata[i].key          = !g_keydata[i].key;
			g_keydata[i].pressed_time = 0;
			g_keydata[i].repeat_time  = 0;

		}else if( !g_keydata[i].key && k_new[i] ){
			//key Up --> Down
			gen_key_event( i, 1, now() );
			g_keydata[i].key          = !g_keydata[i].key;
			g_keydata[i].pressed_time = now();
			g_keydata[i].repeat_time  = now() + 2000;
			//printf( "%lu----KEY DOWN--%lu--\n", now(),g_keydata[i].repeat_time);
		}else if( g_keydata[i].key ){
			//key Keep Down
			if( time_after( now(), g_keydata[i].repeat_time )){
				//gen repeat key
				//printf( "%lu----KEY DOWN REPEAT--%lu--\n", now(),g_keydata[i].repeat_time);
				gen_key_event( i, 2, g_keydata[i].pressed_time );
				g_keydata[i].repeat_time += 500;
			}
		}else{
			//key Keep Up 
		}

	}

	return;
}

