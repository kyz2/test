#include "ui.h"
#include "tem.h"
#include "setting.h"

static setdata_t g_setdata={

    .rw.tem=3,
    .rw.tem_up_mistake=15,
    .rw.tem_dn_mistake=15,
    .rw.tem_sensor_err_open_time=10,
    .rw.tem_sensor_err_close_time=10,
    .rw.tem_compensate=-10,
    .rw.tem_defrosting_cycle=6,
    .rw.tem_defrosting_time=25,
    .rw.tem_fault_code = 0,
    .rw.tem_gprs_signal_intensity = 0,
    .rw.tem_dis_mode=0,
//  .crc32 = xxx;
/*
    .compressor_rest_time_stop_MIN=3,
    .tem_setMAX=6,
    .tem_setMIN=2,
    .tem_HHH_alarm=15,
    .tem_LLL_alarm=-10,
    .tem_overtem_duration_time=1,
    .menu_counter=DELAY_8S,
    .menu_value=MN_DEFAULT,
    .tem_uploadnet_cycle = 30, //30 分钟（仅可通过服务器远程调节）
    .tem_heartbeat_cycle = 30 //10 分钟（仅可通过服务器远程调节）V1.0.11修改成30M
*/
};

void read_setdata_from_nvs( void )
{
#if 0
    nvs_handle handle;
    static const char *DATA1 = "vTaskTemPro_F";
    static const char *DATA2 = "vTaskTem_Param";
    uint32_t temp,len;
    esp_err_t err;
    err = nvs_flash_init();
    if(err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
        // printf("nvs_flash_init: %d\n", 0);
    }
    //printf("a_setdata:%u\r\n", sizeof(a_setdata));

    err = nvs_open("vTaskTemC", NVS_READWRITE, &handle);
    if (err != ESP_OK) {
        //printf("opening NVS Error: %d\n",  0);

    } else {
        nvs_get_u32(handle, DATA1, &temp);
        //printf("get over: %d\n", 0);
        //printf("a_setdata.save_flag: %d\n", temp);
    }
    nvs_close(handle);

    if(temp != 0x5a55a5a5) {
        nvs_write_data_to_flash_init();
        //printf("nvs_write_data_to_flash_init_temp: %d\n", 0);
    } else {
        err = nvs_open("vTaskTemC", NVS_READWRITE, &handle);
        if (err != ESP_OK) {
            //printf("opening NVS Error: %d\n",  0);
        } else {
            len = sizeof(a_setdata);
            nvs_get_blob(handle, DATA2, &a_setdata, &len);

        }
        nvs_close(handle);
    }
#endif
}

void setdata_write_to_nvs(  void )
{
#if 0

#endif
}

int32_t setdata_get_para( uint32_t idx )
{
	switch( idx ){
		case PARA_TEM: return g_setdata.rw.tem*10;
		case PARA_C1: return g_setdata.rw.tem_up_mistake;
		case PARA_C2: return g_setdata.rw.tem_dn_mistake;
		case PARA_C3: return g_setdata.rw.tem_sensor_err_open_time;
		case PARA_C4: return g_setdata.rw.tem_sensor_err_close_time;
		case PARA_C5: return g_setdata.rw.tem_compensate;
		case PARA_D1: return g_setdata.rw.tem_defrosting_cycle;
		case PARA_D2: return g_setdata.rw.tem_defrosting_time;
		case PARA_F1: return g_setdata.rw.tem_fault_code;
		case PARA_F2: return g_setdata.rw.tem_gprs_signal_intensity;
		case PARA_F3: return g_setdata.rw.tem_dis_mode;

		case PARA_COMP_STOP_MIN: return TEM_COMP_STOP_MIN;
		case PARA_TS_MAX: return TEM_MAX;
		case PARA_TS_MIN: return TEM_MIN;
		case PARA_HHH_ALARM: return TEM_HHH_ALARM;
		case PARA_LLL_ALARM: return TEM_LLL_ALARM;
		case PARA_UPLOADNET_CYCLE: return TEM_UPLOADNET_CYCLE;
		case PARA_HEARTBEAT_CYCLE: return TEM_HEARTBEAT_CYCLE; 
	}
	return 0;
}

void setdata_set_para( uint32_t idx, int32_t value )
{
	switch( idx ){
		case PARA_TEM: 
			g_setdata.rw.tem_up_mistake=value;
			break;
		case PARA_C2:
			g_setdata.rw.tem_dn_mistake = value;
			break;
		case PARA_C3:
			g_setdata.rw.tem_sensor_err_open_time=value;
			break;
		case PARA_C4:
			g_setdata.rw.tem_sensor_err_close_time=value;
			break;
		case PARA_C5:
			g_setdata.rw.tem_compensate=value;
			break;
		case PARA_D1:
			g_setdata.rw.tem_defrosting_cycle=value;
			break;
		case PARA_D2:
			g_setdata.rw.tem_defrosting_time=value;
			break;
		case PARA_F3:
			g_setdata.rw.tem_dis_mode= value;
			break;
		default:
			break;
	}
}

