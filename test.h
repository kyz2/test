#ifndef __TEST_H__
#define __TEST_H__

#include <unistd.h>

typedef u_int32_t uint32_t;
typedef u_int8_t uint8_t;
typedef u_int16_t uint16_t;

#endif //__TEST_H__
