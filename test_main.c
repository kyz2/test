#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include <pthread.h>
#include "test.h"
#include "ui.h"

extern disp_info_t  g_dispinfo;

int fd_disp =-1;
int fd_key  =-1;

char *disp_fifo = "/tmp/disp.fifo";
char *key_fifo =  "/tmp/key.fifo";

static uint32_t key_io_st = 0; //0:up, 1:down
static int32_t g_tem_measure = 0;


uint8_t PT6964_Get_Key(void)
{
	return key_io_st;
}

int32_t read_temperature( void )
{
	return g_tem_measure;
}


void* key_input( void * pdata )
{
	unsigned char buf[200];
	int len;
	int read_io;
	int32_t t=0;
	while(1){
		len = read( fd_key, &buf[0], 100 );
		if( len>=5 && len < 100 ){
			buf[len] = 0;
			if( strncmp( buf, "KEY:", 4 ) == 0 ){
				if( buf[4] == '1' )
					key_io_st = 1;
				else if( buf[4] == '0' )
					key_io_st = 0;

			}else if( strncmp( buf, "TEM:", 4 ) == 0 ){
				if( sscanf(  &buf[4], "%d", &t ) == 1)
					g_tem_measure = t;	
			}
		}else{
			printf( "read fifo err:%d\n", len );
		}
	}
	return NULL;
}

void create_fifo( void )
{
	if(0!=access(key_fifo,F_OK)) {
		if(-1 == mkfifo(key_fifo,0777)) {
			fprintf(stderr,"creat key fifo err \n");
			exit(0);
		}
	}

	if(0!=access(disp_fifo,F_OK)) {
		if(-1 == mkfifo(disp_fifo,0777)) {
			fprintf(stderr,"creat disp fifo err \n");
			exit(0);
		}
	}
#if  1
	fd_key = open( key_fifo, O_RDONLY);
	fd_disp = open( disp_fifo, O_WRONLY);

	if( fd_key<0 || fd_disp < 0 )
		exit(0);
#endif
}

int main( void )
{
	pthread_t input_thread;
	pthread_t output_thread;

	create_fifo();
		
	pthread_create( &input_thread, NULL, key_input, NULL );

	ui_task();

	return 0;
}

//-------------------------------------------
//display

uint8_t PT6964_disp(void)
{
	char buf[20];
	
	strcpy( buf, "DISP:" );
	buf[5] = g_dispinfo.led[0]?'*':' ';
	if( g_dispinfo.txt_flash.active && !g_dispinfo.txt_flash.on ){
		buf[6] = ' ';	
		buf[7] = ' ';	
		buf[8] = ' ';	
	}else{
		buf[6] = g_dispinfo.minus?'-':' ';
		buf[7] = g_dispinfo.txt[0];	
		buf[8] = g_dispinfo.txt[1];	
	}
	buf[9] = '\n';
	buf[10] = 0;

	if(0){
		static uint32_t ii = 0;	
		if( (ii++ % 40)==0 )
			printf( "%d:disp refresh: [%s]", now(), buf );
	}

	if( fd_disp > 0 ){
		write( fd_disp, buf, 10 );		
	}
}
#include <sys/time.h>
uint32_t linux_now( void )
{
	struct timeval  tv;
	struct timezone  tz;

	static uint32_t t_last=0;
	uint32_t  t_now;
	uint32_t  ret;

	gettimeofday( &tv, &tz);
	t_now = tv.tv_sec*1000+tv.tv_usec/1000;

	if( t_last == 0 )
		t_last = t_now;

	ret = t_now-t_last;
	ret = ret*2; //for debug
	return ret;
}

int32_t read_tem( void );
uint32_t read_tem_sensor_status( void );

int32_t read_tem( void )
{
	static int32_t t = 20;
	static uint32_t count;

	if( (count++)%20  == 0)
		t++;

	if( t>200 )
		t = -50;

	return t;
}

uint32_t tem_sensor_status( void )
{
	return 0;
}

