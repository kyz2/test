#! /usr/bin/python

import os
import time
 
from tkinter import *
from tkinter import ttk

import threading

imgfile={
	'0' : 'res/0.png',
	'1' : 'res/1.png',
	'2' : 'res/2.png',
	'3' : 'res/3.png',
	'4' : 'res/4.png',
	'5' : 'res/5.png',
	'6' : 'res/6.png',
	'7' : 'res/7.png',
	'8' : 'res/8.png',
	'9' : 'res/9.png',
	'A' : 'res/A.png',
	'b' : 'res/b.png',
	'C' : 'res/C.png',
	'd' : 'res/d.png',
	'E' : 'res/E.png',
	'F' : 'res/F.png',
	'H' : 'res/H.png',
	'L' : 'res/L.png',
	'r' : 'res/r.png',
	'Z' : 'res/Z.png',
	'-' : 'res/minus.png',
	' ' : 'res/off.png',
	'*' : 'res/snow-on.png',
	'x' : 'res/snow-off.png'
}

img = {}

def load_app( win ):
	os.system( "./test" )

def key_fifo( win ):
	while True:
		win.keyfifo = os.open( "/tmp/key.fifo", os.O_WRONLY )
		if win.keyfifo < 0:
			time.sleep(2)
		else:
			break


def disp_fifo( win ):
	disp = os.open( "/tmp/disp.fifo", os.O_RDONLY )
	while True:
		data =  os.read(disp, 10)
		#print( "recv:", len(data) )
		#print(data)
		if data[:5]==b'DISP:' :
			info = data[5:]
			#print( "parse INFO:", info )
			if info[0] !=ord('*'):
				win.lab_star['image']=img['*']
			else:
				win.lab_star['image']=img['x']
			if info[1] == ord('-'):
				win.lab_minus['image']=img['-']
			else:
				win.lab_minus['image']=img[' ']
			if info[2] == ord(' '):
				win.lab_n1['image']=img[' ']
			else:
				win.lab_n1['image']=img[chr(info[2])]
			if info[3] == ord(' '):
				win.lab_n2['image']=img[' ']
			else:
				win.lab_n2['image']=img[chr(info[3])]
			#print( "refresh wiget" )

class app_gui:
	def __init__( self, master ):
		self.root = master

		self.start_time = int( time.time())

		if __name__ == '__main__':
			self.root.title( 'Test' )
			self.root.resizable( False,  False )
		master.columnconfigure(1, weight=1)

		for i in imgfile:
			img[i]=PhotoImage(file=imgfile[i])
			#print( i,img[i], imgfile[i] )
		self.keyfifo = -1
		self.make_widget()
		self.th_disp = threading.Thread( target= disp_fifo,args=[self])
		self.th_key = threading.Thread( target= key_fifo,args=[self])
		self.th_app = threading.Thread( target= load_app,args=[self])

		self.th_disp.start()
		self.th_key.start()
		self.th_app.start()

	def on_set_pressed( self,w ):
		if self.keyfifo>=0:
			os.write( self.keyfifo, b'KEY:0' )

	def on_set_released( self, w ):
		if self.keyfifo>=0:
			os.write( self.keyfifo, b'KEY:1' )

	def on_t_set( self, t ):
		if self.keyfifo>0:
			s = b'TEM:%d'%(self.scale_t_set.get()*10)
			os.write( self.keyfifo, s )

	def on_timer( self ):
		t = int( time.time() ) - self.start_time;
		self.lab_time['text' ] = "%02d:%02d:%02d"%( t/3600, (t%3600)/60, t%60 ) 
		self.lab_time.after( 1000, self.on_timer )
	def make_widget( self ):

		self.root.columnconfigure(1,weight=1)
		self.root.rowconfigure(1, weight=1)

		self.f1 = Frame( self.root )
		self.f2 = Frame( self.root )
		self.f3 = Frame( self.root )
		self.f4 = Frame( self.root )
		self.f1.grid( row = 0, column = 0, sticky="WE" )
		self.f2.grid( row = 1, column = 0, sticky="WE" )
		self.f3.grid( row = 2, column = 0, sticky="WE" )
		self.f4.grid( row = 3, column = 0, sticky="WE" )

		self.f1.columnconfigure(0,weight=1)
		self.f2.columnconfigure(0,weight=1)
		self.f3.columnconfigure(0,weight=1)
		self.f4.columnconfigure(1,weight=1)
		self.f4.columnconfigure(1,weight=1)
		self.f1.rowconfigure(0, weight=1)
		self.f2.rowconfigure(0, weight=1)
		self.f3.rowconfigure(0, weight=1)
		self.f4.rowconfigure(0, weight=1)

		self.lab_time = Label( self.f1, text = '00:00:00' )
		self.lab_time.grid(sticky='WE')
		self.lab_time.after( 1000, self.on_timer )


		self.btn_set  = Button( self.f2, text="SET", bg='green', width=20 )
		self.btn_set.grid( row=0, column=4, sticky="NSWE")

		#display
		self.lab_star = Label( self.f2, image=img['*'], borderwidth=0 )
		self.lab_minus = Label( self.f2, image=img['-'], borderwidth=0 )
		self.lab_n1 = Label( self.f2, image=img['Z'], borderwidth=0  )
		self.lab_n2 = Label( self.f2, image=img['8'], borderwidth=0  )
		self.lab_star.grid(row=0,column=0)
		self.lab_minus.grid(row=0, column=1)
		self.lab_n1.grid(row=0, column=2)
		self.lab_n2.grid(row=0, column=3)

		#temperature set
		self.scale_t_set = Scale( self.f3, label=u'温度', from_ = -10, to = 10, resolution=1, orient=HORIZONTAL, tickinterval= 2 )
		self.scale_t_set.grid( row=0,column=0,sticky="EW")

		#compressor status
		self.lab_comp = Label( self.f4, text=u'压缩机' )
		self.lab_fan = Label( self.f4, text=u'风扇' )
		self.lab_comp.grid( row=0, column = 0 )
		self.lab_fan.grid( row=0, column=1 )

		#button
		self.btn_set.bind( '<ButtonRelease-1>', self.on_set_pressed )
		self.btn_set.bind( '<Button-1>', self.on_set_released )

		self.scale_t_set.bind( '<ButtonRelease-1>', self.on_t_set)

		#self.lab_n1.config( font='Helvetica-60' )
		#self.lab_n2.config( font='Helvetica-60' )
	def on_selet_file( self ):
		pass

	def on_download( self ):
		pass

if __name__ == '__main__':
        m=app_gui( Tk() )
        m.root.mainloop()
