#include "ui.h"

disp_info_t  g_dispinfo;

extern uint8_t PT6964_disp(void);

void disp_str( char* str  )
{
	if( strlen(str) == 2 ){
		g_dispinfo.minus = 0;
		g_dispinfo.txt[0] = str[0];
		g_dispinfo.txt[1] = str[1];
		g_dispinfo.dot[0] = 0;
		g_dispinfo.dot[1] = 0;
	}
}

void disp_int( int32_t data )
{
	if( data<0)
		g_dispinfo.minus = 1;
	else
		g_dispinfo.minus = 0;

	g_dispinfo.txt[1] = '0' + (abs(data)%10);
	g_dispinfo.txt[0] = '0' + ((abs(data)/10)%10);
	if( g_dispinfo.txt[0] == '0')
		g_dispinfo.txt[0] = ' ';

	g_dispinfo.dot[0] = 0;
	g_dispinfo.dot[1] = 0;
}

void disp_txt_flash_start( uint32_t delay_time, uint32_t on_time, uint32_t off_time )
{
	g_dispinfo.txt_flash.active = 1;
	g_dispinfo.txt_flash.on = 1;
	g_dispinfo.txt_flash.delay_time= delay_time;
	g_dispinfo.txt_flash.on_time= on_time;
	g_dispinfo.txt_flash.off_time= off_time;
	g_dispinfo.txt_flash.start_time = now();
	g_dispinfo.txt_flash.toggle_time= now() + delay_time;
}

void disp_txt_flash_stop( void )
{
	g_dispinfo.txt_flash.active = 0;
}

void disp_led_on( uint32_t idx )
{
	g_dispinfo.led_flash[idx].active = 0;
	g_dispinfo.led[idx] = 1;
}

void disp_led_off( uint32_t idx )
{
	g_dispinfo.led_flash[idx].active = 0;
	g_dispinfo.led[idx] = 0;
}

void disp_led_flash( uint32_t idx,  uint32_t delay_time, uint32_t on_time, uint32_t off_time )
{
	g_dispinfo.led_flash[idx].active = 1;
	g_dispinfo.led[idx] = 1;
	g_dispinfo.led_flash[idx].delay_time= delay_time;
	g_dispinfo.led_flash[idx].on_time= on_time;
	g_dispinfo.led_flash[idx].off_time= off_time;
	g_dispinfo.led_flash[idx].start_time = now();
	g_dispinfo.led_flash[idx].toggle_time= now() + delay_time;
}

void disp_flash_toggle( void )
{
	int i;
	disp_flash_time_t *p_flash;

	if( g_dispinfo.txt_flash.active ){

		p_flash = &g_dispinfo.txt_flash;
		if( time_after( now(), p_flash->toggle_time ) ){
			p_flash->on = !p_flash->on;
			p_flash->toggle_time += p_flash->on?p_flash->on_time:p_flash->off_time;
		}
	}
	for( i=0; i<LED_NUM; i++ ){
		if( g_dispinfo.led_flash[i].active ){

			p_flash = &g_dispinfo.led_flash[i];
			if( time_after( now(), p_flash->toggle_time ) ){
				g_dispinfo.led[i] = !g_dispinfo.led[i];
				p_flash->toggle_time += p_flash->on?p_flash->on_time:p_flash->off_time;
			}
		}
	}
}

void disp_refresh( void )
{
	PT6964_disp();
}

