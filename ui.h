#ifndef __UI_H__
#define __UI_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "test.h"

//-----------------------------------
//key


#define KEY_RELEASED 0
#define KEY_PRESSED  1
#define KEY_REPEAT   2

#define  time_after(unknown,known)           ((int32_t)(known) - (int32_t)(unknown)<0)
#define  time_before(unkonwn,known)         ((int32_t)(unknown) - (int32_t)(known)<0)

enum{
	KEY_SET   = 0,
	KEY_PLUS  = 1,
	KEY_MINUS = 2,
	KEY_NUM
};

typedef struct{
	struct evt{
		uint32_t code;
		uint32_t st;
		uint32_t keep_time;
	} evt[KEY_NUM];
} key_event_t;

extern key_event_t g_keyevent;

uint32_t get_key_event( key_event_t *k );
void key_timer_handler( void );

//-----------------------------------
//display
#define LED_NUM 2

typedef struct{
	uint32_t active; //0: disable, 1: enable
	uint32_t on;   //0: off, 1:on
	uint32_t delay_time;  //first on time; 
	uint32_t on_time;
	uint32_t off_time;
	uint32_t start_time;
	uint32_t toggle_time;
} disp_flash_time_t;

typedef struct{
	uint8_t txt[2];
	uint8_t dot[2];
	uint8_t minus;
	uint8_t led[2]; 
	disp_flash_time_t txt_flash;
	disp_flash_time_t led_flash[LED_NUM];
} disp_info_t;

void disp_refresh( void );
void disp_str( char* str );
void disp_int( int32_t data );
void disp_txt_flash_start( uint32_t delay_time, uint32_t on_time, uint32_t off_time );
void disp_txt_flash_stop( void );
void disp_led_on( uint32_t idx );
void disp_led_off( uint32_t idx );
void disp_led_flash( uint32_t idx,  uint32_t delay_time, uint32_t on_time, uint32_t off_time );
void disp_flash_toggle( void );

//-----------------------------------
//window

typedef void (*win_handler_t)( uint32_t ev, key_event_t *pdata );
#define ARRAY_SIZE( a )  ( sizeof(a)/sizeof(a[1] ) )

typedef enum{
	MAIN_W = 0,  //show tem
	T_SET_W = 1, //tem setting
	P_SET_W = 2, //parameter setting
	P_C1_W,
	P_C2_W,
	P_C3_W,
	P_C4_W,
	P_C5_W,
	P_D1_W,
	P_D2_W,
	P_F1_W,
	P_F2_W,
	P_F3_W,
	ALARM_W,
	OFF_W,
	W_COUNT
}win_id_t;

typedef struct{
	win_id_t id;
	char *   txt;
	uint32_t timeout;
}sub_win_t;

typedef struct{
	win_id_t   id;
	win_handler_t handler;
	uint32_t  timeout;
} win_t;


//--------------------------------------------------------//
#define EV_INIT  1
#define EV_EXIT  2
#define EV_KEY   3
#define EV_TIMER 4

//--------------------------------------------------------//
void ui_init( void );
void ui_task( void );

uint32_t now( void );
#endif //__UI_H__
