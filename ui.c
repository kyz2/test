#include "ui.h"
#include "tem.h"
#include "setting.h"

static void para_store( win_id_t idx, int32_t val, int32_t min_v, int32_t max_v );
static void para_load( win_id_t idx, int32_t *val, int32_t * min_v, int32_t *max_v );

extern int32_t g_wait_key_released[3];
typedef uint32_t event_t;
static void main_handle( event_t ev, key_event_t *k );
static void tem_set_handle( event_t ev, key_event_t *k );
static void para_set_handle( event_t ev, key_event_t *k  );
static void para_handle( event_t ev, key_event_t *k );
static void start_win( win_id_t win );
static uint32_t is_win_timeout( win_id_t win );

static win_id_t g_curr;
static win_t g_win[] = {
	{ MAIN_W,  main_handle, },
	{ T_SET_W, tem_set_handle, },
	{ P_SET_W, para_set_handle, },
	{ P_C1_W,  para_handle, },
	{ P_C2_W,  para_handle, },
	{ P_C3_W,  para_handle, },
	{ P_C4_W,  para_handle, },
	{ P_C5_W,  para_handle, },
	{ P_D1_W,  para_handle, },
	{ P_D2_W,  para_handle, },
	{ P_F1_W,  para_handle, },
	{ P_F2_W,  para_handle, },
	{ P_F3_W,  para_handle, },
};


static struct para_item{
	win_id_t win;
	char *  name;
} para_list[] = {
	{ P_C1_W, "C1" },
	{ P_C2_W, "C2" },
	{ P_C3_W, "C3" },
	{ P_C4_W, "C4" },
	{ P_C5_W, "C5" },
	{ P_D1_W, "d1" },
	{ P_D2_W, "d2" },
	{ P_F1_W, "F1" },
	{ P_F2_W, "F2" },
	{ P_F3_W, "F3" },
};

uint32_t now( void )
{
	extern uint32_t linux_now( void );
	return linux_now();
}

static void para_load( win_id_t idx, int32_t *val, int32_t * min_v, int32_t *max_v )
{
	switch( idx ){
		case P_C1_W:
			//*val = set.c1.v;
			*val = 3;
			*min_v = 2;
			*max_v = 6;
			break;
		case P_C2_W:
			*val = 7;
			*min_v = 4;
			*max_v = 8;
			break;
		case P_C3_W:
			*val = 6;
			*min_v = 0;
			*max_v = 100;
			break;
	}
}
static void para_store( win_id_t idx, int32_t val, int32_t min_v, int32_t max_v )
{
	switch( idx ){
		case P_C1_W:
			//*val = set.c1.v;
			break;
		case P_C2_W:
			break;
		case P_C3_W:
			break;
	}
}

extern tem_disp_t g_temdisp;

static void main_handle( uint32_t ev, key_event_t *k )
{
	switch(ev){
		case EV_INIT:
			//show tem
			//TODO
			disp_txt_flash_stop();
			//disp_int( g_temdisp.disp_tem/10 );
			break;
		case EV_KEY:

			if( k->evt[KEY_SET].code == 1){
				if( k->evt[KEY_SET].st == KEY_RELEASED 
						&& k->evt[KEY_SET].keep_time < 2000 ){

					start_win( T_SET_W );
				}else if( k->evt[KEY_SET].st == KEY_REPEAT
						&& k->evt[KEY_SET].keep_time >= 8000 ){
					g_wait_key_released[KEY_SET] = 1;
					start_win( P_SET_W );

				}
			}
			break;
		case EV_EXIT:
			return;

		default:
			break;
	}
	disp_int( g_temdisp.disp_tem/10 );
}

static void tem_set_handle( event_t ev, key_event_t *k )
{
	static int32_t t=0;
	switch(ev){
		case EV_INIT:
			g_win[g_curr].timeout = now()+8000;
			//TODO : show t_set_val
			//t = get_set_t();
			//t = 3;
			printf( "---- T SET: %d ---\n", t );
			disp_int( t/10 );
			disp_txt_flash_start( 1000, 500, 500 );
			break;

		case EV_KEY:

			if( k->evt[KEY_SET].code == 1){
				g_win[g_curr].timeout = now()+8000;
				if( k->evt[KEY_SET].st == KEY_RELEASED 
						&& k->evt[KEY_SET].keep_time < 2000 ){

					t++;
					printf( "------ADD, t=%d ----\n", t );
					disp_int( t/10 );
					disp_txt_flash_start( 1000, 500, 500 );
					if( t>6)
						t= -2;

					//TODO
					//add/sub t
					//display t
				}
			}
			break;		

		case EV_EXIT:
			disp_txt_flash_stop();
			return;

		default:
			break;		
	}
	if( is_win_timeout( g_curr ) )
		start_win( MAIN_W );
}

static void para_set_handle( event_t ev, key_event_t *k )
{
	static uint32_t idx;
	switch(ev){
		case EV_INIT:
			g_win[g_curr].timeout= now()+8000;
			idx=0;
			disp_str(para_list[ idx ].name );
			disp_txt_flash_start( 1000, 500, 500 );
			break;

		case EV_KEY:

			if( k->evt[KEY_SET].code == 1){
				if( k->evt[KEY_SET].st == KEY_RELEASED 
						&& k->evt[KEY_SET].keep_time < 2000 ){

					idx++;
					if( idx >= sizeof(para_list)/sizeof(para_list[0]) )
						idx=0;
					
					g_win[g_curr].timeout = now()+8000;
					disp_str(para_list[ idx ].name );
					disp_txt_flash_start( 1000, 500, 500 );

				}else if( k->evt[KEY_SET].st == KEY_REPEAT
						&& k->evt[KEY_SET].keep_time >= 5000 ){

					start_win( para_list[idx].win );

				}
			}
			break;

		case EV_EXIT:
			//TODO save value;
			return;		
		case EV_TIMER:
		default:
			break;		
	}	
	if( is_win_timeout( g_curr ) )
		start_win( MAIN_W );
}

static void para_handle( event_t ev, key_event_t *k )
{
	static int32_t idx=8;
	static int32_t max_v=10;
	static int32_t min_v=5;

	switch(ev){
		case EV_INIT:
			g_win[g_curr].timeout= now()+8000;
			//TODO read_para( g_curr, &idx, &max_v, &min_v ); 
			//TODO disp_int( idx );
			disp_int( idx );
			disp_txt_flash_start( 100, 500, 500 );
			break;

		case EV_KEY:
			if( k->evt[KEY_SET].code ){
				if( ( k->evt[KEY_SET].st == KEY_RELEASED 
							&& k->evt[KEY_SET].keep_time < 2000 )
						|| ( k->evt[KEY_SET].st == KEY_REPEAT
							&& k->evt[KEY_SET].keep_time >= 2000 ) ){
					idx++;
					if( idx >= max_v )
						idx = min_v;
					g_win[g_curr].timeout = now()+8000;
					disp_int( idx );
					disp_txt_flash_start( 100, 500, 500 );


				}
			}
			break;
		case EV_EXIT:
			//save value;
			//TODO set_write_c1(idx);
			return;	
		case EV_TIMER:
		default:
			break;		
	}
	if( is_win_timeout( g_curr ) )
		start_win( P_SET_W );
}

static void start_win( win_id_t win )
{

	printf( "---  win:%d -- > %d ---\n", g_curr, win  );
	if( g_curr )
		g_win[g_curr].handler( EV_EXIT, 0 );
	g_curr = win;
	g_win[g_curr].handler( EV_INIT, 0 );
}

static uint32_t is_win_timeout( win_id_t win )
{
	return time_after( now(), g_win[g_curr].timeout );
}

//int32_t is_key_set_short_release( key_ev_t *k );
//int32_t is_key_set_long_pressed_8s( key_ev_t *k );
//void check_timeout( win_id parent );


void ui_task( void )
{
	uint32_t t;

	key_event_t k;

	temdisp_adjust_init();
	start_win( T_SET_W );

	t = now()+100;
	t = now()+100;
	while(1){

		//tem_timer_handler();
		//adc_timer_handler();
		temdisp_adjust();
		key_timer_handler();
		get_key_event( &k );
		g_win[g_curr].handler( EV_KEY, &k );
		disp_refresh();
		disp_flash_toggle();
		while( !time_after( now(), t ) )
			usleep(10);
		t+=50;
	}
}

